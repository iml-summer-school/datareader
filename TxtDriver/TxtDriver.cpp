#include "TxtDriver.hpp"
#include "..\mylibrary\Channel.hpp"
#include <fstream>
#include <queue>

void simple()
{
	return;
}

std::shared_ptr<IDriver> getDriver()
{
	return std::make_shared<TxtDriver>();
}

class DataAdapter : public IDataAdapter
{
public:
	DataAdapter(std::queue<double> data) : m_data{ std::move(data) } {}

	double readNext() override
	{
		if (m_data.empty())
		{
			throw std::logic_error("No more data.");
		}
		auto ret = m_data.front();
		m_data.pop();
		return ret;
	}

private:
	std::queue<double> m_data;
};

void readField(std::ifstream& in, std::string& field)
{
	if (in)
	{
		in >> field;
	}
	else
	{
		throw std::logic_error("Missing input data");
	}
}

Channel TxtDriver::read(std::string file)
{
	// make sure file is .txt
	std::ifstream in(file);
	std::string name, unit;
	std::queue<double> data;

	readField(in, name);
	readField(in, unit);

	while (in.good())
	{
		double val;
		in >> val;
		data.push(val);
	}
	int numOfSamples = data.size();

	return Channel{ std::move(name), std::move(unit), numOfSamples , new DataAdapter(std::move(data)) };
}
