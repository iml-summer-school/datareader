#pragma once
#include "..\mylibrary\IDriver.hpp"
#include <memory>

#ifdef BUILD_DLL
#define	 PORTABLE_DLL_DECL __declspec(dllexport)
#else
#define PORTABLE_DLL_DECL __declspec(dllimport)
#endif

class Channel;

class TxtDriver : public IDriver
{
public:
	Channel read(std::string file) override;
};

template class std::shared_ptr<IDriver>;

extern "C" PORTABLE_DLL_DECL void simple();
extern "C" PORTABLE_DLL_DECL std::shared_ptr<IDriver> getDriver();