#include "mylibrary\Interface.h"
#include "TxtDriver\TxtDriver.hpp"
#include <iostream>

int main()
{
	RegisterDll("txt", R"(C:\Users\iosifzota\source\repos\datareader\x64\Debug\TxtDriver.dll)");

	auto channel = GetChannel("Text.txt");

	while (channel.hasNextSample())
	{
		std::cout << channel.nextSample() << std::endl;
	}

	return 0;
}