#pragma once

#ifdef BUILD_DLL
#define	 PORTABLE_DLL_DECL __declspec(dllexport)
#else
#define PORTABLE_DLL_DECL __declspec(dllimport)
#endif

#include "Channel.hpp"
#include <string>

PORTABLE_DLL_DECL Channel GetChannel(const std::string& fileName);
PORTABLE_DLL_DECL void RegisterDll(const std::string& fileExt, const std::string& name);