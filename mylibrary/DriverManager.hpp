#pragma once
#include <map>
#include <memory>
#include "DriverRegister.hpp"

class IDriver;

typedef  std::shared_ptr<IDriver> (*GetDriverFn)();

class DriverManager {
public:
	void registerDriver(std::string fileExe, std::string dllName)
	{
		if (m_drivers.find(fileExe) != m_drivers.end())
		{
			throw std::invalid_argument("The dll you try to register is already loadede");
		}
		DriverRegister dll(dllName);
		m_drivers.emplace(fileExe, dll);
	}

	std::shared_ptr<IDriver> getDriver(std::string fileExe)
	{
		if (m_drivers.find(fileExe) != m_drivers.end())
		{
			// make sure function name is not mangled
			GetDriverFn getDriver = (GetDriverFn)GetProcAddress(m_drivers[fileExe].getDll(), "getDriver");
			return (*getDriver)();
		}
		else
			throw std::invalid_argument("The file you chose is not suported by the program ;");
	}
private:
	std::map<std::string, DriverRegister> m_drivers;
};