#pragma once
#include <string>


class Channel;

class IDriver
{
public:
	virtual Channel read(std::string file) = 0;
};