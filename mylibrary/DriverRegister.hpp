#pragma once
#include <string>
#include <Windows.h>

class IDriver;

class DriverRegister
{
public:
	DriverRegister() : m_modulue{ nullptr }
	{
		// empty
	}

	DriverRegister(std::string name) :m_dllName(name), m_modulue{ nullptr } {
	};
	HINSTANCE getDll() 
	{
		if (!m_modulue)
		{
			m_modulue = LoadLibraryA(m_dllName.c_str());
		}
		return m_modulue;
	}
	~DriverRegister()
	{
		if (m_modulue)
		{
			FreeLibrary(m_modulue);
		}
	}
private:
	std::string m_dllName;
	HINSTANCE m_modulue;
};