#include "Utility.h"

#include<regex>

std::string Utility::praseExtension(std::string fileName)
{
	static std::regex reg(R"(\w+\.\w+)");
	if (!std::regex_match(fileName, reg))
	{
		throw std::domain_error("Bad file name.");
	}
	return std::string(std::find(std::begin(fileName), std::end(fileName), '.') + 1, std::end(fileName));
}