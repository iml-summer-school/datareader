#pragma once
#include <string>
#include "IDataAdapter.hpp"

class Channel
{
public:
	Channel(std::string name, std::string unit, int numOfSamples, IDataAdapter * samples) :
		m_name(std::move(name)), m_unit(std::move(unit)), m_numOfSamples(numOfSamples), m_samples(samples) {}
	~Channel() {
		delete m_samples;
	}

	bool hasNextSample()
	{
		return m_numOfSamples != 0;
	}

	double nextSample()
	{
		--m_numOfSamples;
		return m_samples->readNext();
	}
	
private:
	IDataAdapter * m_samples;
	std::string m_name;
	std::string m_unit;
	int m_numOfSamples;
};
