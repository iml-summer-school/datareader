#pragma once
class IDataAdapter
{
public:
	virtual double readNext() = 0;

};