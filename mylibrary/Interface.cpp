#include "Interface.h"
#include "DriverManager.hpp"
#include "Channel.hpp"
#include "Utility.h"
#include "IDriver.hpp"

DriverManager& getDriverManager()
{
	static DriverManager dm;
	return dm;
}

Channel GetChannel(const std::string& fileName)
{
	auto driver = getDriverManager().getDriver(Utility::praseExtension(fileName));
	return driver->read(fileName);
}

void RegisterDll(const std::string & fileExt, const std::string & name)
{
	return getDriverManager().registerDriver(fileExt, name);
}